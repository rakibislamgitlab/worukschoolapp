package com.bsoftpsv.woruksch.viewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.bsoftpsv.woruksch.pojos.BudgetPojo;
import com.bsoftpsv.woruksch.repository.BudgetRepositoryDB;

import java.util.List;

public class BudgetViewModel extends ViewModel {
    private BudgetRepositoryDB budgetRepositoryDB;
    public MutableLiveData<List<BudgetPojo>> budgetlist=new MutableLiveData<>();
    public MutableLiveData<BudgetPojo>budgetdetailsMlD=new MutableLiveData<>();


    public BudgetViewModel() {
        budgetRepositoryDB=new BudgetRepositoryDB();
        budgetlist=budgetRepositoryDB.budgetlist;

    }
    public  void saveBudget(BudgetPojo budgetPojo){
        budgetRepositoryDB.saveBudgetRepository(budgetPojo);
    }
    public  void getBudgetalldetalis(String eventid){
        budgetdetailsMlD=budgetRepositoryDB.getBudgetdetailsMlD(eventid);
    }
    public  void deleteBudgetlist(BudgetPojo budgetPojo){
        budgetRepositoryDB.deletebudgetRepository(budgetPojo);
    }
}
