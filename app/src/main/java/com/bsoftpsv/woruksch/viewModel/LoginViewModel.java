package com.bsoftpsv.woruksch.viewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.bsoftpsv.woruksch.repository.LoginFirebaseRepository;

public class LoginViewModel extends ViewModel implements LoginFirebaseRepository.UserAuthomticationStates {
private LoginFirebaseRepository loginFirebaseRepository;
    public enum AuthenticationState{
        AUTHENTICATED,
        UNAUTHENTICATED

    }
    public MutableLiveData<AuthenticationState> authenticationStateMLD;
    public  MutableLiveData<String>errMsgLd;
    public LoginViewModel(){
            authenticationStateMLD=new MutableLiveData<>();
            errMsgLd=new MutableLiveData<>();
            loginFirebaseRepository=new LoginFirebaseRepository(this);
            if (loginFirebaseRepository.getFirebaseUser() !=null){
                authenticationStateMLD.setValue(AuthenticationState.AUTHENTICATED);
            }
            else {
                authenticationStateMLD.setValue(AuthenticationState.UNAUTHENTICATED);
            }

    }
    public void  loginuser(String email,String password){
        loginFirebaseRepository.loginRepository(email,password);

    }
    public  void resiteruser(String email,String password){
        loginFirebaseRepository.resisterRepository(email,password);

    }



    @Override
    public void onAuthSucess() {
        authenticationStateMLD.setValue(AuthenticationState.AUTHENTICATED);

    }

    @Override
    public void onAuthFailure(String msg) {
        authenticationStateMLD.setValue(AuthenticationState.UNAUTHENTICATED);
        errMsgLd.postValue(msg);

    }
}
