package com.bsoftpsv.woruksch.viewModel;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.bsoftpsv.woruksch.pojos.ContracPojo;
import com.bsoftpsv.woruksch.repository.ContractRepositoryDB;

import java.util.List;

public class ContractViewModel extends ViewModel {
    private  ContractRepositoryDB contractRepositoryDB;

    public MutableLiveData<List<ContracPojo>> contractListLD=new MutableLiveData<>();
    public MutableLiveData<ContracPojo>contractDetailsMLD=new MutableLiveData<>();


    public ContractViewModel(){
       contractRepositoryDB=new ContractRepositoryDB();
       contractListLD=contractRepositoryDB.contractListLD;
    }

    public  void savecontract(ContracPojo contracPojo){
        contractRepositoryDB.saveContractFirebaseRepository(contracPojo);
    }

    public  void deletecontract(ContracPojo contracPojo){
        contractRepositoryDB.deleteContracttRepository(contracPojo);
    }

}
