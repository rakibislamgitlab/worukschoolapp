package com.bsoftpsv.woruksch.viewModel;


import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.bsoftpsv.woruksch.pojos.EventPojo;
import com.bsoftpsv.woruksch.repository.EventRepositoryDB;

import java.util.List;

public class EventViewModel extends ViewModel {
    private EventRepositoryDB eventRepositoryDB;
    public MutableLiveData<List<EventPojo>> eventListLD=new MutableLiveData<>();
    public MutableLiveData<EventPojo>eventDetails=new MutableLiveData<>();

    public  EventViewModel(){
        eventRepositoryDB=new EventRepositoryDB();
        eventListLD=eventRepositoryDB.eventListLD;
    }

    public  void  saveEvent(EventPojo eventPojo){
        eventRepositoryDB.saveEventFirebaseRepository(eventPojo);
    }

    public  void getDatailsEvent(String eventID){
        eventDetails=eventRepositoryDB.getEventDetailsByID(eventID);
    }

    public void updateEvent(EventPojo eventPojo){
        eventRepositoryDB.updateEventRB(eventPojo);
    }

    public void deleteEvent(EventPojo eventPojo){
        eventRepositoryDB.deleteEventRB(eventPojo);
    }

    }
