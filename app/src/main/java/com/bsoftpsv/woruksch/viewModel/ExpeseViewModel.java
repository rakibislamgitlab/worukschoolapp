package com.bsoftpsv.woruksch.viewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


import com.bsoftpsv.woruksch.pojos.Expense;
import com.bsoftpsv.woruksch.repository.ExpenseRepository;

import java.util.List;

public class ExpeseViewModel extends ViewModel {
    private ExpenseRepository expenseRepository;
    public MutableLiveData<List<Expense>> expenseListMLD =new MutableLiveData<>();
    public ExpeseViewModel(){
        expenseRepository=new ExpenseRepository();
        expenseListMLD=expenseRepository.expenseListMLD;
    }
    public void saveExpense(Expense expense){
        expenseRepository.saveExpenseToRDR(expense);
    }
    public void getExpenses(String eventId){
        expenseListMLD = expenseRepository.getAllExpensesByEventId(eventId);
    }
}
