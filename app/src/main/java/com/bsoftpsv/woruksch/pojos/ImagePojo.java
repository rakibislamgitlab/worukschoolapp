package com.bsoftpsv.woruksch.pojos;

import android.widget.ImageView;

public class ImagePojo {
   private  int imageUrl;


    public ImagePojo() {

    }

    public ImagePojo(int imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(int imageUrl) {
        this.imageUrl = imageUrl;
    }
}
