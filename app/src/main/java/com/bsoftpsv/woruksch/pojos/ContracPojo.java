package com.bsoftpsv.woruksch.pojos;

public class ContracPojo {
    private  String contractId;
    private  String studentName;
    private  String studentBlood;
    private  String batch;
    private  String occupation;
    private  String phonenumber;
    private  String adress;

    public ContracPojo() {
    }

    public ContracPojo(String contractId, String studentName, String studentBlood,
                       String batch, String occupation, String phonenumber, String adress) {
        this.contractId = contractId;
        this.studentName = studentName;
        this.studentBlood = studentBlood;
        this.batch = batch;
        this.occupation = occupation;
        this.phonenumber = phonenumber;
        this.adress = adress;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentBlood() {
        return studentBlood;
    }

    public void setStudentBlood(String studentBlood) {
        this.studentBlood = studentBlood;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }
}
