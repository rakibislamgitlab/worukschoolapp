package com.bsoftpsv.woruksch.pojos;

public class BudgetPojo {
    private String budgetId;
    private String batchssc;
    private String unity_name;
    private String giverName;
    private int budget;
    private String dateCurrent;

    public BudgetPojo() {
        //firbase
    }

    public BudgetPojo(String budgetId,
                      String batchssc, String unity_name, String giverName, int budget, String dateCurrent) {
        this.budgetId = budgetId;
        this.batchssc = batchssc;
        this.unity_name = unity_name;
        this.giverName = giverName;
        this.budget = budget;
        this.dateCurrent = dateCurrent;
    }

    public String getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(String budgetId) {
        this.budgetId = budgetId;
    }

    public String getBatchssc() {
        return batchssc;
    }

    public void setBatchssc(String batchssc) {
        this.batchssc = batchssc;
    }

    public String getUnity_name() {
        return unity_name;
    }

    public void setUnity_name(String unity_name) {
        this.unity_name = unity_name;
    }

    public String getGiverName() {
        return giverName;
    }

    public void setGiverName(String giverName) {
        this.giverName = giverName;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public String getDateCurrent() {
        return dateCurrent;
    }

    public void setDateCurrent(String dateCurrent) {
        this.dateCurrent = dateCurrent;
    }
}
