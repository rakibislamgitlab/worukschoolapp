package com.bsoftpsv.woruksch.pojos;

public class Expense {
    private String expenseId;
    private String eventId;
    private String expenseName;
    private int expenseAmount;
    private String expenseDateTime;

    public Expense() {
    }

    public Expense(String expenseId, String eventId, String expenseName, int expenseAmount, String expenseDateTime) {
        this.expenseId = expenseId;
        this.eventId = eventId;
        this.expenseName = expenseName;
        this.expenseAmount = expenseAmount;
        this.expenseDateTime = expenseDateTime;
    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }

    public int getExpenseAmount() {
        return expenseAmount;
    }

    public void setExpenseAmount(int expenseAmount) {
        this.expenseAmount = expenseAmount;
    }

    public String getExpenseDateTime() {
        return expenseDateTime;
    }

    public void setExpenseDateTime(String expenseDateTime) {
        this.expenseDateTime = expenseDateTime;
    }
}
