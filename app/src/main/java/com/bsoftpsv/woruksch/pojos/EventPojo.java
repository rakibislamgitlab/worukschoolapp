package com.bsoftpsv.woruksch.pojos;

public class EventPojo {
    private  String eventId;
    private  String eventName;
    private  String locaton;
    private  String date;
    private  int initialBudget;
    private String createEventDate;

    public EventPojo() {
    }

    public EventPojo(String eventId, String eventName, String locaton, String date, int initialBudget, String createEventDate) {
        this.eventId = eventId;
        this.eventName = eventName;
        this.locaton = locaton;
        this.date = date;
        this.initialBudget = initialBudget;
        this.createEventDate = createEventDate;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getLocaton() {
        return locaton;
    }

    public void setLocaton(String locaton) {
        this.locaton = locaton;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getInitialBudget() {
        return initialBudget;
    }

    public void setInitialBudget(int initialBudget) {
        this.initialBudget = initialBudget;
    }

    public String getCreateEventDate() {
        return createEventDate;
    }

    public void setCreateEventDate(String createEventDate) {
        this.createEventDate = createEventDate;
    }
}
