package com.bsoftpsv.woruksch;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bsoftpsv.woruksch.databinding.FragmentAdminLoginBinding;
import com.bsoftpsv.woruksch.viewModel.LoginViewModel;

@SuppressWarnings("ALL")
/**
 * A simple {@link Fragment} subclass.
 */
public class AdminLoginFragment extends Fragment {
    private FragmentAdminLoginBinding binding;
    private LoginViewModel loginViewModel;


    public AdminLoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding=FragmentAdminLoginBinding.inflate(LayoutInflater.from(getActivity()));
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loginViewModel= ViewModelProviders.of(this).get(LoginViewModel.class);
        loginViewModel.authenticationStateMLD.observe(this, new Observer<LoginViewModel.AuthenticationState>() {
            @Override
            public void onChanged(LoginViewModel.AuthenticationState authenticationState) {
                switch (authenticationState){
                    case AUTHENTICATED:
                        Navigation.findNavController(view).navigate(R.id.admindashboardFragment);
                        break;
                    case UNAUTHENTICATED:
                        break;
                }
            }
        });
        loginViewModel.errMsgLd.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                binding.statusTV.setText(s);
            }
        });
        binding.loginBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email=binding.emailET.getText().toString();
                String password=binding.passwordET.getText().toString();
                if (email.isEmpty()){
                    binding.emailET.setError("Enter An Email");
                    binding.emailET.requestFocus();
                    return;
                }
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                    binding.emailET.setError(" Enter A valid Email");
                    binding.emailET.requestFocus();
                    return;
                }
                if (password.isEmpty()){
                    binding.passwordET.setError(" Enter a Password");
                    binding.passwordET.requestFocus();
                    return;
                }
                if (password.length()<6){
                    binding.passwordET.setError("Mimimum 6 number password");
                    binding.passwordET.requestFocus();
                    return;
                }
                loginViewModel.loginuser(email,password);


            }
        });
        binding.resisterBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email=binding.emailET.getText().toString();
                String password=binding.passwordET.getText().toString();
                if (email.isEmpty()){
                    binding.emailET.setError("Enter An Email");
                    binding.emailET.requestFocus();
                    return;
                }
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                    binding.emailET.setError(" Enter A valid Email");
                    binding.emailET.requestFocus();
                    return;
                }
                if (password.isEmpty()){
                    binding.passwordET.setError(" Enter a Password");
                    binding.passwordET.requestFocus();
                    return;
                }
                if (password.length()<6){
                    binding.passwordET.setError("Mimimum 6 number password");
                    binding.passwordET.requestFocus();
                    return;
                }
                loginViewModel.resiteruser(email,password);

            }
        });
    }

}
