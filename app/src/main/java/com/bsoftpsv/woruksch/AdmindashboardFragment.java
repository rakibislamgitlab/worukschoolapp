package com.bsoftpsv.woruksch;


import android.app.AlertDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bsoftpsv.woruksch.databinding.FragmentAdminLoginBinding;
import com.bsoftpsv.woruksch.databinding.FragmentAdmindashboardBinding;
import com.bsoftpsv.woruksch.databinding.FragmentEventDashBoardBinding;
import com.google.firebase.auth.FirebaseAuth;


/**
 * A simple {@link Fragment} subclass.
 */
public class AdmindashboardFragment extends Fragment {
    private FragmentAdmindashboardBinding binding;


    public AdmindashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("        ADMIN DASHBOARD");
        setHasOptionsMenu(true);
        binding=FragmentAdmindashboardBinding.inflate(LayoutInflater.from(getActivity()));
        // Inflate the layout for this fragment
        return binding.getRoot();
    }
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.logout_manu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case  R.id.item_logout:
                LayoutInflater inflater=LayoutInflater.from(getActivity());
                View view=inflater.inflate(R.layout.logout,null);
                TextView yes=view.findViewById(R.id.yesBtn);
                TextView no =view.findViewById(R.id.noBtn);
                AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
                builder.setView(view);
                final   AlertDialog dialog= builder.create();
                dialog.show();
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FirebaseAuth.getInstance().signOut();
                        //   startActivity(new Intent(getActivity(), MainActivity.class));
                        Navigation.findNavController(getActivity(),R.id.fragment)
                                .navigate(R.id.adminLoginFragment);
                        dialog.dismiss();
                    }
                });
                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.cancel();
                    }
                });


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.budgetbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.action_admindashboardFragment_to_student_budgetFragment2);
            }
        });
        binding.contractbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.action_admindashboardFragment_to_batchContractList);
            }
        });
//        binding.newsBT.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Navigation.findNavController(view).navigate(R.id.action_admindashboardFragment_to_newsFragment);
//            }
//        });
        binding.eventbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.action_admindashboardFragment_to_event_List_Fragment2);
            }
        });
    }
}
