package com.bsoftpsv.woruksch;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bsoftpsv.woruksch.databinding.FragmentAddContractBinding;
import com.bsoftpsv.woruksch.databinding.FragmentSmsHscBinding;


/**
 * A simple {@link Fragment} subclass.
 */
public class SmsHSCFragment extends Fragment {
    private String examss = "";
    private String boardss = "";
    private String yearss="";
private FragmentSmsHscBinding binding;
    public SmsHSCFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       binding=FragmentSmsHscBinding.inflate(LayoutInflater.from(getActivity()));

        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(getContext(),R.array.exam
                ,android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        binding.examSP.setAdapter(spinnerAdapter);

        ArrayAdapter<CharSequence> spinnerAdapterrr = ArrayAdapter.createFromResource(getContext(),R.array.board
                ,android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        binding.boardSp.setAdapter(spinnerAdapterrr);

        ArrayAdapter<CharSequence> spinnerAdapteryear = ArrayAdapter.createFromResource(getContext(),R.array.year
                ,android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        binding.yearSP.setAdapter(spinnerAdapteryear);

       return  binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);




        binding.examSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                examss= adapterView.getItemAtPosition(i).toString();
                Toast.makeText(getActivity(), examss, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.boardSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                boardss= adapterView.getItemAtPosition(i).toString();
                Toast.makeText(getActivity(), boardss, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.yearSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                yearss= adapterView.getItemAtPosition(i).toString();
                Toast.makeText(getActivity(), yearss, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        binding.sendBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String PhoneNumber = "16222";
                String sms = binding.rollEt.getText().toString();

            String add=( examss+" "+ boardss+" "+sms+" "+yearss);

                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("smsto:" + PhoneNumber));  // This ensures only SMS apps respond
               intent.putExtra("sms_body",add);
                // intent.putExtra(Intent.EXTRA_STREAM, attachment);
                if (intent.resolveActivity(getContext().getPackageManager()) != null) {
                    getContext().startActivity(intent);
                }

            }
        });

    }
}

