package com.bsoftpsv.woruksch.repository;

import androidx.annotation.NonNull;

import com.bsoftpsv.woruksch.viewModel.LoginViewModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginFirebaseRepository {
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private  UserAuthomticationStates lisenar;

    public   LoginFirebaseRepository(LoginViewModel loginViewModel){
        firebaseAuth=FirebaseAuth.getInstance();
        firebaseUser=firebaseAuth.getCurrentUser();
        lisenar=loginViewModel;

    }
    public void loginRepository(String email,String password){
        firebaseAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    firebaseUser=firebaseAuth.getCurrentUser();
                    lisenar.onAuthSucess();

                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                lisenar.onAuthFailure(e.getLocalizedMessage());

            }
        });

    }
    public   void  resisterRepository(String email,String password){
        firebaseAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    firebaseUser=firebaseAuth.getCurrentUser();
                    lisenar.onAuthSucess();

                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                lisenar.onAuthFailure(e.getLocalizedMessage());

            }
        });

    }
    public  interface  UserAuthomticationStates{
        void onAuthSucess();
        void  onAuthFailure(String msg);

    }
    public  FirebaseUser getFirebaseUser(){
        return  firebaseUser;
    }
}
