package com.bsoftpsv.woruksch.repository;


import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.bsoftpsv.woruksch.pojos.BudgetPojo;
import com.bsoftpsv.woruksch.pojos.EventPojo;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class EventRepositoryDB {
    private FirebaseUser firebaseUser;
    private DatabaseReference rootRef;
    private  DatabaseReference userRef;
    private DatabaseReference eventRef;
    public MutableLiveData<List<EventPojo>> eventListLD=new MutableLiveData<>();
    public MutableLiveData<EventPojo>eventDetails=new MutableLiveData<>();

    public  EventRepositoryDB(){
        firebaseUser= FirebaseAuth.getInstance().getCurrentUser();

        rootRef= FirebaseDatabase.getInstance().getReference();
        userRef=rootRef.child(firebaseUser.getUid());
        eventRef=userRef.child("Event");
        eventRef.keepSynced(true);


        eventRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<EventPojo>events=new ArrayList<>();
                for (DataSnapshot d : dataSnapshot.getChildren()){
                    events.add(d.getValue(EventPojo.class));
                }
                eventListLD.setValue(events);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public  void  saveEventFirebaseRepository(EventPojo eventPojo){
        String eventId=eventRef.push().getKey();
        eventPojo.setEventId(eventId);

        eventRef.child(eventId).setValue(eventPojo).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }
    public  MutableLiveData<EventPojo>getEventDetailsByID(String eventID){
        eventRef.child(eventID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                EventPojo eventPojo= dataSnapshot.getValue(EventPojo.class);
                eventDetails.postValue(eventPojo);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return  eventDetails;
    }
    public  void updateEventRB(EventPojo eventPojo){
        String eventId=eventPojo.getEventId();
        eventPojo.setEventId(eventId);
        eventRef.child(eventId).setValue(eventPojo).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });


    }
    public  void  deleteEventRB(EventPojo eventPojo){
        String eventId=eventPojo.getEventId();
        eventPojo.setEventId(eventId);
        eventRef.child(eventId).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }



}
