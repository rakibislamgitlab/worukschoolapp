package com.bsoftpsv.woruksch.repository;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.bsoftpsv.woruksch.adptar.ContractRVA;
import com.bsoftpsv.woruksch.pojos.ContracPojo;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.core.Context;

import java.util.ArrayList;
import java.util.List;

public class ContractRepositoryDB {
    private ContractRVA contractRVA;

    private FirebaseUser firebaseUser;
    private DatabaseReference rootRef;
    private  DatabaseReference userRef;
    private DatabaseReference contractRef;
    public MutableLiveData<List<ContracPojo>> contractListLD=new MutableLiveData<>();
    public MutableLiveData<ContracPojo>contractDetailsMLD=new MutableLiveData<>();


    public ContractRepositoryDB() {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        rootRef = FirebaseDatabase.getInstance().getReference();
        userRef = rootRef.child(firebaseUser.getUid());
        contractRef = userRef.child("Contract");
        contractRef.keepSynced(true);

        contractRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<ContracPojo> contract = new ArrayList<>();

           //   contractRVA.updateList(contract);


                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    contract.add(d.getValue(ContracPojo.class));
                }
                contractListLD.setValue(contract);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
        public void saveContractFirebaseRepository (ContracPojo contracPojo){
            String contractid = contractRef.push().getKey();
            contracPojo.setContractId(contractid);

            contractRef.child(contractid).setValue(contracPojo).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });
        }
    public  void  deleteContracttRepository(ContracPojo contracPojo){
        String  contractId=contracPojo.getContractId();
        contracPojo.setContractId(contractId);
        contractRef.child(contractId).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }


}