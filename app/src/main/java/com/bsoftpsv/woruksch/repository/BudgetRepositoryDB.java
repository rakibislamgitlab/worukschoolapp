package com.bsoftpsv.woruksch.repository;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.bsoftpsv.woruksch.pojos.BudgetPojo;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class BudgetRepositoryDB {
    private FirebaseUser firebaseUser;
    private DatabaseReference rootRef;
    private  DatabaseReference userRef;
    private DatabaseReference budgettRef;
    public MutableLiveData<List<BudgetPojo>>budgetlist=new MutableLiveData<>();
    public MutableLiveData<BudgetPojo>budgetdetailsMlD=new MutableLiveData<>();



    public BudgetRepositoryDB(){
        firebaseUser= FirebaseAuth.getInstance().getCurrentUser();
        rootRef= FirebaseDatabase.getInstance().getReference();
        userRef=rootRef.child(firebaseUser.getUid());
        budgettRef=userRef.child("Budget List");
        budgettRef.keepSynced(true);

 budgettRef.addValueEventListener(new ValueEventListener() {
     @Override
     public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
         List<BudgetPojo>budgetPojos=new ArrayList<>();
         for (DataSnapshot d: dataSnapshot.getChildren()){
             budgetPojos.add(d.getValue(BudgetPojo.class));
         }
         budgetlist.setValue(budgetPojos);

     }

     @Override
     public void onCancelled(@NonNull DatabaseError databaseError) {

     }
 });

    }
    public  void saveBudgetRepository(BudgetPojo budgetPojo ){
        String budgetID=budgettRef.push().getKey();
        budgetPojo.setBudgetId(budgetID);
budgettRef.child(budgetID).setValue(budgetPojo).addOnSuccessListener(new OnSuccessListener<Void>() {
    @Override
    public void onSuccess(Void aVoid) {

    }
}).addOnFailureListener(new OnFailureListener() {
    @Override
    public void onFailure(@NonNull Exception e) {

    }
});


    }
    public  MutableLiveData<BudgetPojo>getBudgetdetailsMlD(String eventId){
        budgettRef.child(eventId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                BudgetPojo budgetPojo=dataSnapshot.getValue(BudgetPojo.class);
                budgetdetailsMlD.postValue(budgetPojo);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return budgetdetailsMlD;
    }
    public void deletebudgetRepository(BudgetPojo budgetPojo){
        String budgetId=budgetPojo.getBudgetId();
        budgetPojo.setBudgetId(budgetId);
        budgettRef.child(budgetId).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });

    }
}
