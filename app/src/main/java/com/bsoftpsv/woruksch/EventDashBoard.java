package com.bsoftpsv.woruksch;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bsoftpsv.woruksch.adptar.ExpenseRBAdapter;
import com.bsoftpsv.woruksch.databinding.AddExpenseDailogBinding;
import com.bsoftpsv.woruksch.pojos.EventPojo;
import com.bsoftpsv.woruksch.databinding.FragmentEventDashBoardBinding;
import com.bsoftpsv.woruksch.pojos.Expense;
import com.bsoftpsv.woruksch.viewModel.EventViewModel;
import com.bsoftpsv.woruksch.viewModel.ExpeseViewModel;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
@SuppressWarnings("ALL")

/**
 * A simple {@link Fragment} subclass.
 */
public class EventDashBoard extends Fragment {

private FragmentEventDashBoardBinding binding;
    private int totalBudget = 0;
    private String eventID;
    private EventViewModel eventViewModel;
private EventPojo eventPojo;
private ExpeseViewModel expeseViewModel;
    private NumberFormat nf = new DecimalFormat("##.###");
    List<Expense>expenseList;
    private ExpenseRBAdapter expenseRBAdapter;
    public EventDashBoard() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("              Event Dash Board");
        setHasOptionsMenu(true);
        eventViewModel= ViewModelProviders.of(this).get(EventViewModel.class);
        expeseViewModel=ViewModelProviders.of(this).get(ExpeseViewModel.class);
        Bundle bundle = getArguments();
        if (bundle != null) {
            eventID = bundle.getString("id");
         //   Toast.makeText(getActivity(), eventID, Toast.LENGTH_SHORT).show();
            eventViewModel.getDatailsEvent(eventID);
            expeseViewModel.getExpenses(eventID);
        }

        binding=FragmentEventDashBoardBinding.inflate(LayoutInflater.from(getActivity()));
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        eventViewModel.eventDetails.observe(this, new Observer<EventPojo>() {
            @Override
            public void onChanged(EventPojo eventPojo) {
               //eventPojo= eventPojo;
                totalBudget = eventPojo.getInitialBudget();
               binding.eventNameTV.setText(eventPojo.getEventName());
               binding.budgetTV.setText("বাজেট : " + eventPojo.getInitialBudget() + "/- টাকা");

            }
        });



        expeseViewModel.expenseListMLD.observe(this, new Observer<List<Expense>>() {
            @Override
            public void onChanged(List<Expense> expenses) {
                expenseList=expenses;

                int totalEx = 0;
                int remaining = 0;


                for (Expense expensePojo : expenses) {
                    totalEx += expensePojo.getExpenseAmount();
                }
                remaining = totalBudget - totalEx;

             binding.expenseTV.setText("মোট খরচ : " + totalEx + "/- টাকা");
                //remainingTV.setText("Remaining  " + remaining+" টাকা");

              binding.  currenBalanceDisplayTvId.setText("বর্তমান ব্যালেন্স : " + remaining + "/- টাকা");

                double consumed2 = (Double.valueOf(totalEx) * 100) / Double.valueOf(totalBudget);
            binding.expensePersentageTvId .setText(String.valueOf(nf.format(consumed2)) + "%");

            binding.budExTvId.setText(totalEx + "/" + totalBudget);

                showProgressBar(totalEx, totalBudget);



            }
        });

        createProgressBar();




        binding.expenseAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addExpense();
            }
        });
        expeseViewModel.expenseListMLD.observe(this, new Observer<List<Expense>>() {
            @Override
            public void onChanged(List<Expense> expenses) {
                expenseRBAdapter = new ExpenseRBAdapter(getActivity(),expenseList);
                LinearLayoutManager llm=new LinearLayoutManager(getActivity());
                binding.expensRV.setLayoutManager(llm);
                binding.expensRV.setAdapter(expenseRBAdapter);
            }
        });

    }


    private void showProgressBar(int totalEx, int budget) {
        if (totalEx >= 0) {
            calculateProgress(totalEx, budget);
        } else
            Toast.makeText(getContext(), "Sorry! No Ammount is remainnig.", Toast.LENGTH_SHORT).show();
    }
    private void calculateProgress(int totalEx, int budget) {
        if (totalEx >= 0) {

            //consumed = (expenditure * 100) / budget;
            double consumed3 = (Double.valueOf(totalEx) * 100) / Double.valueOf(budget);
        binding.progressBar.setProgress(Integer.valueOf((int) consumed3));

            if (consumed3 >= 90) {
             binding.progressBar.setProgressTintList(ColorStateList.valueOf(Color.parseColor("#FCE70F0F")));
            } else if (consumed3 >= 70) {
               binding.progressBar.setProgressTintList(ColorStateList.valueOf(Color.parseColor("#FF9800")));
            } else if (consumed3 >= 50) {
             binding.progressBar.setProgressTintList(ColorStateList.valueOf(Color.parseColor("#2ecc71")));
            }

        } else Toast.makeText(getContext(), "please enter some ammount", Toast.LENGTH_SHORT).show();
    }


    private void createProgressBar() {
        binding.progressBar.setIndeterminate(false);
        binding.progressBar.setMax(100);
        binding.progressBar.showContextMenu();
        binding.progressBar.setScaleY(5f);
    }



    private void addExpense() {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setTitle("       --------Add Expense-------");
        final AddExpenseDailogBinding binding= DataBindingUtil.inflate(LayoutInflater
                .from(getActivity()),R.layout.add_expense_dailog,null,true);
        builder.setView(binding.getRoot());

        builder.setPositiveButton(" Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String amount=binding.addExpenseAmountInput.getText().toString();
                String comment=binding.addExpenseCommentInput.getText().toString();

                if (amount.isEmpty()){
                    binding.addExpenseAmountInput.setError("Add Amaount");
                    binding.addExpenseAmountInput.requestFocus();
                    return;
                }
                if (comment.isEmpty()){
                    binding.addExpenseCommentInput.setError("Add Comment");
                    binding.addExpenseCommentInput.requestFocus();
                    return;
                }

                Expense expense=new Expense(null,eventID,comment,Integer.parseInt(amount),EventUtils.getCurrentDateTime());
                expeseViewModel.saveExpense(expense);


            }
        });
        builder.setNegativeButton("Cencel",null);
        AlertDialog alertDialog=builder.create();
        alertDialog.show();

    }
}
