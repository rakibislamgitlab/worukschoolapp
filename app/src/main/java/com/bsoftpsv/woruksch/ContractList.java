package com.bsoftpsv.woruksch;


import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.bsoftpsv.woruksch.adptar.ContractRVA;


import com.bsoftpsv.woruksch.databinding.FragmentContractListBinding;
import com.bsoftpsv.woruksch.pojos.ContracPojo;
import com.bsoftpsv.woruksch.viewModel.ContractViewModel;

import java.util.List;
import java.util.zip.Inflater;

@SuppressWarnings("ALL")
/**
 * A simple {@link Fragment} subclass.
 */
public class ContractList extends Fragment {
private FragmentContractListBinding binding;
private ContractViewModel contractViewModel;
private ContractRVA contractRVA;

    public ContractList() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("        STUDENT CONTRACT LIST");
        setHasOptionsMenu(true);
        contractViewModel= ViewModelProviders.of(this).get(ContractViewModel.class);
        binding=FragmentContractListBinding.inflate(LayoutInflater.from(getActivity()));
        // Inflate the layout for this fragment
        return binding.getRoot();
    }





    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.addContractBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view) .navigate(R.id.action_batchContractList_to_add_Contract_Fragment);
            }
        });
        contractViewModel.contractListLD.observe(this, new Observer<List<ContracPojo>>() {
            @Override
            public void onChanged(List<ContracPojo> contracPojos) {
                contractRVA=new ContractRVA(getActivity(),contracPojos);
                LinearLayoutManager llm=new LinearLayoutManager(getActivity());
                binding.contractRRRV.setLayoutManager(llm);
                binding.contractRRRV.setHasFixedSize(true);
                binding.contractRRRV.setItemViewCacheSize(10);
                binding.contractRRRV.setDrawingCacheEnabled(true);
                binding.contractRRRV.setAdapter(contractRVA);
                binding.contractProgress.setVisibility(View.GONE);
               // contractRVA.updateList(contracPojos);

            }
        });
        binding.contractRRRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {

                switch (newState){
                    case RecyclerView.  SCROLL_STATE_IDLE:
                        binding.addContractBtn.show();
                        break;
                    default:
                        binding.addContractBtn.hide();
                        break;
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }


        @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {

        inflater.inflate(R.menu.sarch_manu,menu);

        // Associate searchable configuration with the SearchView
//        SearchManager searchManager = (SearchManager)getActivity().getSystemService(Context.SEARCH_SERVICE);
//        SearchView searchView= (SearchView) menu.findItem(R.id.searchid)
//                .getActionView();
//        assert searchManager != null;
//        searchView.setSearchableInfo(searchManager
//                .getSearchableInfo(getActivity().getComponentName()));
//        searchView.setMaxWidth(Integer.MAX_VALUE);
            MenuItem item=menu.findItem(R.id.searchid);
            final SearchView searchView=(SearchView) MenuItemCompat.getActionView(item);
        searchView.setQueryHint("Search by Batch");
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
               contractRVA.getFilter().filter(query);


                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                contractRVA.getFilter().filter(newText);

                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);

    }


}
