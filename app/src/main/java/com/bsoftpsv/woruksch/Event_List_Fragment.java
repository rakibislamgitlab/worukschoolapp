package com.bsoftpsv.woruksch;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.bsoftpsv.woruksch.pojos.EventPojo;
import com.bsoftpsv.woruksch.adptar.EventRVA;
import com.bsoftpsv.woruksch.databinding.FragmentEventListFragment2Binding;
import com.bsoftpsv.woruksch.viewModel.EventViewModel;

import java.util.List;

@SuppressWarnings("All")
/**
 * A simple {@link Fragment} subclass.
 */
public class Event_List_Fragment extends Fragment {
private FragmentEventListFragment2Binding binding;
private EventViewModel eventViewModel;
private EventRVA eventRVA;

    public Event_List_Fragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
//        (getActivity()).getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        eventViewModel= ViewModelProviders.of(this).get(EventViewModel.class);
    // Inflate the layout for this fragment
      getActivity().setTitle("            EVENT LIST");
   setHasOptionsMenu(true);
   binding=FragmentEventListFragment2Binding.inflate(LayoutInflater.from(getActivity()));
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.addEventBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.action_event_List_Fragment2_to_addEventFragment);
            }
        });
        eventViewModel.eventListLD.observe(this, new Observer<List<EventPojo>>() {
            @Override
            public void onChanged(List<EventPojo> eventPojos) {
                eventRVA=new EventRVA(getActivity(),eventPojos);
                LinearLayoutManager llm=new LinearLayoutManager(getActivity());
                binding.eventRV.setLayoutManager(llm);
                binding.eventRV.setHasFixedSize(true);
                binding.eventRV.setItemViewCacheSize(6);
                binding.eventRV.setDrawingCacheEnabled(true);
                binding.eventRV.setAdapter(eventRVA);
                binding.eventProgress.setVisibility(View.GONE);



            }
        });
        binding.eventRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {

                switch (newState){
                    case RecyclerView.  SCROLL_STATE_IDLE:
                        binding.addEventBtn.show();
                        break;
                        default:
                            binding.addEventBtn.hide();
                            break;
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

}


