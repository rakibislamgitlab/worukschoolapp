package com.bsoftpsv.woruksch.ui.gallery;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bsoftpsv.woruksch.R;
import com.bsoftpsv.woruksch.adptar.ImageRVA;
import com.bsoftpsv.woruksch.databinding.FragmentGalleryBinding;
import com.bsoftpsv.woruksch.pojos.ImagePojo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GalleryFragment extends Fragment {

private FragmentGalleryBinding binding;


ArrayList<ImagePojo>imagePojoArrayList;




    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

       binding=FragmentGalleryBinding.inflate(LayoutInflater.from(getActivity()));

       gridLIst();


       binding. recylcycalview.setHasFixedSize(true);

        LinearLayoutManager llm=new GridLayoutManager(getActivity(),1);
        ImageRVA imageRVA=new ImageRVA(getActivity(),imagePojoArrayList);
        binding.recylcycalview.setLayoutManager(llm);
        binding.recylcycalview.setAdapter(imageRVA);




        return binding.getRoot();
    }

    private void gridLIst() {
        imagePojoArrayList=new ArrayList<>();
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic1));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic2));
        imagePojoArrayList.add(new ImagePojo(R.drawable.waruk));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic3));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic4));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic5));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic2));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic6));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic7));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic8));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic10));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic9));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic12));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic11));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic13));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic14));
        imagePojoArrayList.add(new ImagePojo(R.drawable.schoolpic1));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic15));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic16));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic17));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic18));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic19));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic20));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic21));
        imagePojoArrayList.add(new ImagePojo(R.drawable.pic22));


    }
}