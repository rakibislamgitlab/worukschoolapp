package com.bsoftpsv.woruksch.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.bsoftpsv.woruksch.R;
import com.bsoftpsv.woruksch.SecondActivity;
import com.bsoftpsv.woruksch.databinding.FragmentHomeBinding;

@SuppressWarnings("ALL")
public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        binding=FragmentHomeBinding.inflate(LayoutInflater.from(getActivity()));

//////////////////////////////////////////
//        initControls();
//        setNotificationData(getActivity().getIntent().getExtras());

        return binding.getRoot();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    //    private void initControls() {
//
//    }
//    private void setNotificationData(Bundle extras) {
//        if (extras == null)
//            return;
//        StringBuilder text = new StringBuilder("");
//        text.append("Message Details:");
//        text.append("\n");
//        text.append("\n");
//        if (extras.containsKey("title")) {
//            text.append("Title: ");
//            text.append(extras.get("title"));
//        }
//        text.append("\n");
//        if (extras.containsKey("message")) {
//            text.append("Message: ");
//            text.append(extras.get("message"));
//        }
//        binding.homeText.setText(text);
//    }



    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.item_login:
                startActivity(new Intent(getActivity(), SecondActivity.class));

                break;
        }
        return false;
    }
}