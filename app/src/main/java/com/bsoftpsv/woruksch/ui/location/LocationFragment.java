package com.bsoftpsv.woruksch.ui.location;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bsoftpsv.woruksch.R;
import com.bsoftpsv.woruksch.databinding.FragmentLocationBinding;


/**
 * A simple {@link Fragment} subclass.
 */
public class LocationFragment extends Fragment {
    private FragmentLocationBinding binding;
    private String location="Waruk Rahamania High School";


    public LocationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding=FragmentLocationBinding.inflate(LayoutInflater.from(getActivity()));
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.mapbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Uri locationuri=Uri.parse("geo:0,0?q="+location);
                Intent locationIntent=new Intent(Intent.ACTION_VIEW,locationuri);
                if (locationIntent.resolveActivity(getActivity().getPackageManager())!=null){
                    startActivity(locationIntent);
                }else {
                    Toast.makeText(getActivity(), "componet found", Toast.LENGTH_SHORT).show();
                }
            }

        });
    }
}
