package com.bsoftpsv.woruksch;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bsoftpsv.woruksch.databinding.FragmentAddStudentBudgetBinding;
import com.bsoftpsv.woruksch.pojos.BudgetPojo;
import com.bsoftpsv.woruksch.viewModel.BudgetViewModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class Add_Student_Budget_Fragment extends Fragment {
    private FragmentAddStudentBudgetBinding binding;
    private BudgetViewModel budgetViewModel;


    public Add_Student_Budget_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("     ADD STUDENT BUDGET");
        setHasOptionsMenu(true);
        budgetViewModel= ViewModelProviders.of(this).get(BudgetViewModel.class);
        binding=FragmentAddStudentBudgetBinding.inflate(LayoutInflater.from(getActivity()));
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.studentsaveBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String batch=binding.batchET.getText().toString();
                String unity=binding.unityEt.getText().toString();
                String gbName=binding.giverET.getText().toString();
                String budget=binding.budgetET.getText().toString();
                if (batch.isEmpty()) {
                    binding.batchET.setError("Enter Batch");
                    return;
                }
                if (unity.isEmpty()) {
                    binding.unityEt.setError("Enter Unity name");
                    return;
                }
                if (gbName.isEmpty()) {
                    binding.giverET.setError("Enter giver name");
                    return;
                }
                if (budget.isEmpty()) {
                    binding.budgetET.setError("Enter add budget");
                    return;
                }
                BudgetPojo budgetPojo=new BudgetPojo(null,batch,unity,gbName,Integer.parseInt(budget),EventUtils.getCurrentDateTime());
                budgetViewModel.saveBudget(budgetPojo);
                Navigation.findNavController(view).navigate(R.id.student_budgetFragment2);
            }
        });


    }
}
