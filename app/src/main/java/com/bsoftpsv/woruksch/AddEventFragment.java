package com.bsoftpsv.woruksch;


import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.bsoftpsv.woruksch.pojos.EventPojo;
import com.bsoftpsv.woruksch.databinding.FragmentAddEventBinding;
import com.bsoftpsv.woruksch.viewModel.EventViewModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 */
@SuppressWarnings("ALL")
public class AddEventFragment extends Fragment {
private FragmentAddEventBinding binding;
private EventViewModel eventViewModel;
    private String eventDate;
    private String eventID=null;

    public AddEventFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding=FragmentAddEventBinding.inflate(LayoutInflater.from(getActivity()));
        eventViewModel= ViewModelProviders.of(this).get(EventViewModel.class);
        Bundle bundle = getArguments();
        if (bundle != null) {

            eventID = bundle.getString("id");

            eventViewModel.getDatailsEvent(eventID);
        }
            eventViewModel.eventDetails.observe(this, new Observer<EventPojo>() {
                @Override
                public void onChanged(EventPojo eventPojo) {
                 binding.eventNameET.setText(eventPojo.getEventName());
                 binding.startLocationET.setText(eventPojo.getLocaton());
                 binding.DateET.setText(eventPojo.getDate());
                 binding.budgetET.setText(String.valueOf(eventPojo.getInitialBudget()));
                    eventDate = eventPojo.getDate();
                    binding.createEventbtn.setVisibility(View.GONE);
                    binding.upateEventBtn.setVisibility(View.VISIBLE);

                }
            });

            return binding.getRoot();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setHasOptionsMenu(true);
       getActivity().setTitle("                       ADD EVENT");
//        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
//        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
binding.createEventbtn.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        String eventName=binding.eventNameET.getText().toString();
        String location=binding.startLocationET.getText().toString();
        String budget=binding.budgetET.getText().toString();
        if (eventName.isEmpty()){
            binding.eventNameET.setError("Enter Event Name");
            return;
        }
        if (location.isEmpty()){
            binding.startLocationET.setError("Enter Location");
            return;
        }
        if (budget.isEmpty()){
            binding.budgetET.setError("Enter Budget");
            return;
        }
        EventPojo eventPojo=new EventPojo(null,eventName,location
                ,eventDate,Integer.parseInt(budget),EventUtils.getDateWithTime());
        eventViewModel.saveEvent(eventPojo);
        Navigation.findNavController(view).navigate(R.id.event_List_Fragment2);
    }
});




        binding.upateEventBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String eventName=binding.eventNameET.getText().toString();
                String location=binding.startLocationET.getText().toString();
                String budget=binding.budgetET.getText().toString();
//                if (eventName.isEmpty()){
//                    binding.eventNameET.setError("Enter Event Name");
//                    return;
//                }
//                if (location.isEmpty()){
//                    binding.startLocationET.setError("Enter Location");
//                    return;
//                }
//                if (budget.isEmpty()){
//                    binding.budgetET.setError("Enter Budget");
//                    return;
//                }
                EventPojo eventPojo=new EventPojo(eventID,eventName,location
                        ,eventDate,Integer.parseInt(budget),EventUtils.getDateWithTime());
                eventViewModel.updateEvent(eventPojo);
                Navigation.findNavController(view).navigate(R.id.event_List_Fragment2);
            }
        });

binding.DateET.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        showDatepicardaialog();
    }
});
    }

    private void showDatepicardaialog() {
        Calendar calendar = Calendar.getInstance();
        int year=calendar.get(Calendar.YEAR);
        int month=calendar.get(Calendar.MONTH);
        int day=calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd= new DatePickerDialog(getActivity(),dateSetlLisenar,year,month,day);
        dpd.show();
    }
    private  DatePickerDialog.OnDateSetListener dateSetlLisenar=new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
            Calendar calendar=Calendar.getInstance();
            calendar.set(year,month,dayOfMonth);
            eventDate=new SimpleDateFormat("dd/MM/yyyy").format(calendar.getTime());
        binding.DateET.setText(eventDate);

        }
    };



}
