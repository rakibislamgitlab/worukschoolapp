package com.bsoftpsv.woruksch.adptar;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import androidx.recyclerview.widget.RecyclerView;

import com.bsoftpsv.woruksch.R;
import com.bsoftpsv.woruksch.databinding.ContractRowBinding;
import com.bsoftpsv.woruksch.pojos.ContracPojo;
import com.bsoftpsv.woruksch.viewModel.ContractViewModel;
import com.google.android.gms.common.internal.Objects;


import java.util.ArrayList;
import java.util.List;

public class ContractRVA extends RecyclerView.Adapter<ContractRVA.ContractViewHolder>implements  Filterable {
    private Context context;
    private List<ContracPojo>contractList;
    private  List<ContracPojo>filtercontractlist;
    private ContractViewModel contractViewModel=new ContractViewModel();

    private ContractRowBinding binding;
    private String PhoneNumber = null;


    public ContractRVA(Context context, List<ContracPojo> contractList) {
        this.context = context;
        this.contractList = contractList;
        filtercontractlist=contractList;
    }

    @NonNull
    @Override
    public ContractViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding= DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.contract_row,parent,false);
        return new ContractViewHolder(binding.getRoot());

    }

    @Override
    public void onBindViewHolder(@NonNull ContractViewHolder holder, final int position) {

       binding.nameEt.setText("Name :"+filtercontractlist.get(position).getStudentName());
       binding.bloodEt.setText("Blood Group :"+filtercontractlist.get(position).getStudentBlood());
       binding.batchET.setText("!! Batch :"+filtercontractlist.get(position).getBatch());
       binding.numberET.setText("Phone :"+filtercontractlist.get(position).getPhonenumber());
       binding.adressET.setText("Adress :"+filtercontractlist.get(position).getAdress());
       PhoneNumber=filtercontractlist.get(position).getPhonenumber();


          holder.itemView.setOnClickListener(new View.OnClickListener() {
              String contractId = contractList.get(position).getContractId();
              final ContracPojo contracPojo = contractList.get(position);



              @Override
              public void onClick(View view) {
                  final Bundle bundle=new Bundle();
                  bundle.putString("id",contractId);
                  Toast.makeText(context, contractId, Toast.LENGTH_SHORT).show();
//                 contractViewModel.deletecontract(contracPojo);

              }
          });

             binding.callBt.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {

                      Uri uriphone=Uri.parse("tel:"+PhoneNumber);
                      Intent intentphonenumber= new Intent(Intent.ACTION_DIAL,uriphone);
                      if (intentphonenumber.resolveActivity(context.getPackageManager()) !=null){
                         context.startActivity(intentphonenumber);
                      }else {
                          Toast.makeText(context, "no component found", Toast.LENGTH_SHORT).show();
                      }
                  }
              });


             binding.messageBt.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {

                      Intent intent = new Intent(Intent.ACTION_SENDTO);
                      intent.setData(Uri.parse("smsto:" + PhoneNumber));  // This ensures only SMS apps respond
                      //intent.putExtra("sms_body", "Hello how are you");
                      // intent.putExtra(Intent.EXTRA_STREAM, attachment);
                      if (intent.resolveActivity(context.getPackageManager()) != null) {
                          context.startActivity(intent);
                      }
                  }
              });



    }

    @Override
    public int getItemCount() {
        return filtercontractlist.size();
    }


@Override
public Filter getFilter() {
    return new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            String query = charSequence.toString();
            if (query.isEmpty()){
                filtercontractlist = contractList;
            }else{
                List<ContracPojo> tempList = new ArrayList<>();
                for (ContracPojo m : contractList){
                    if (m.getStudentBlood().toLowerCase().contains(query.toLowerCase()) ||
                            m.getBatch().contains(query))
                        tempList.add(m);

                    {
                            Toast.makeText(context,""+ m.getBatch()+m.getStudentBlood(), Toast.LENGTH_SHORT).show();

                    }
                }
                filtercontractlist = tempList;
            }
            FilterResults results = new FilterResults();
            results.values = filtercontractlist;
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            filtercontractlist = (List<ContracPojo>) filterResults.values;
            notifyDataSetChanged();
        }
    };
}

    public class ContractViewHolder extends RecyclerView.ViewHolder {
        public ContractViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
//
////    public void updateList(List<ContracPojo> contracPojos){
////        this.contractList = contracPojos;
////        this.filtercontractlist = contracPojos;
////        notifyDataSetChanged();
////    }
//
//
//    @Override
//    public Filter getFilter() {
//        return exmapleFilter;
//    }
//    private Filter exmapleFilter=new Filter() {
//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//         List<ContracPojo>filteredlist=new ArrayList<>();
//         if ( constraint == null || constraint.length() == 0){
//             filteredlist.addAll(filtercontractlist);
//
//         }else {
//             String filterPattern=constraint.toString().toLowerCase().trim();
//             for (ContracPojo pojo: filtercontractlist){
//                 if (pojo.getBatch().toLowerCase().contains(filterPattern)){
//                     filteredlist.add(pojo);
//                 }
//             }
//         }
//         FilterResults results=new FilterResults();
//         results.values=filteredlist;
//         return  results;
//        }
//
//        @Override
//        protected void publishResults(CharSequence constraint, FilterResults results) {
//            contractList.clear();
//            contractList.addAll((List<ContracPojo>) results.values);
//            notifyDataSetChanged();
//
//        }
//    };
}
