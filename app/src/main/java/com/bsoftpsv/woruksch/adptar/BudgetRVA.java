package com.bsoftpsv.woruksch.adptar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bsoftpsv.woruksch.R;
import com.bsoftpsv.woruksch.databinding.BudgetRowBinding;
import com.bsoftpsv.woruksch.pojos.BudgetPojo;
import com.bsoftpsv.woruksch.viewModel.BudgetViewModel;

import java.util.List;

public class BudgetRVA extends RecyclerView.Adapter<BudgetRVA.BudgetViewHolder>{
    private BudgetViewModel budgetViewModel;
    private Context context;
    private List<BudgetPojo>budgetPojoList;
    private BudgetRowBinding binding;

    public BudgetRVA(Context context, List<BudgetPojo> budgetPojoList) {
        this.context = context;
        this.budgetPojoList = budgetPojoList;
    }

    @NonNull
    @Override
    public BudgetViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding= DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.budget_row,parent,false);
        return new BudgetViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull BudgetViewHolder holder, final int position) {
        binding.batchrow.setText("Batch Name :"+budgetPojoList.get(position).getBatchssc());
        binding.unityrow.setText("Unity Name :"+budgetPojoList.get(position).getUnity_name());
        binding.giverrow.setText("Giver Name :"+budgetPojoList.get(position).getGiverName());
        binding.budgetrow.setText("add Budget :"+budgetPojoList.get(position).getBudget());
        binding.dateRow.setText("Date :"+budgetPojoList.get(position).getDateCurrent());
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                String budgetId=budgetPojoList.get(position).getBudgetId();
                final BudgetPojo budgetPojo=budgetPojoList.get(position);
                budgetViewModel.deleteBudgetlist(budgetPojo);
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return budgetPojoList.size();
    }

    public class BudgetViewHolder extends RecyclerView.ViewHolder{
        public BudgetViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

}
