package com.bsoftpsv.woruksch.adptar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.bsoftpsv.woruksch.R;
import com.bsoftpsv.woruksch.databinding.ImageRowBinding;
import com.bsoftpsv.woruksch.pojos.ImagePojo;
import com.google.firebase.storage.internal.Util;

import java.util.List;

public class ImageRVA extends  RecyclerView.Adapter<ImageRVA.ImageViewHolder>{
private Context context;
private List<ImagePojo>imagePojoList;
private ImageRowBinding binding;

    public ImageRVA(Context context, List<ImagePojo> imagePojoList) {
        this.context = context;
        this.imagePojoList = imagePojoList;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding= DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.image_row,parent,false);

        return new ImageViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
      //  binding.imageId.
       binding.imageId.setImageResource(imagePojoList.get(position).getImageUrl());

    }

    @Override
    public int getItemCount() {
        return imagePojoList.size();
    }

    public  class  ImageViewHolder extends ViewHolder {
        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
