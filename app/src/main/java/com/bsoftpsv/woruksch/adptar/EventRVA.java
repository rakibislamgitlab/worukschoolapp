package com.bsoftpsv.woruksch.adptar;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bsoftpsv.woruksch.EventUtils;
import com.bsoftpsv.woruksch.R;
import com.bsoftpsv.woruksch.databinding.EventRowBinding;
import com.bsoftpsv.woruksch.pojos.EventPojo;
import com.bsoftpsv.woruksch.viewModel.EventViewModel;


import java.util.List;

public class EventRVA extends RecyclerView.Adapter {
    private Context context;
    private List<EventPojo>eventPojoList;
    private EventRowBinding binding;
    EventViewModel eventViewModel= new EventViewModel();

    public EventRVA(Context context, List<EventPojo> eventPojoList) {
        this.context = context;
        this.eventPojoList = eventPojoList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding= DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.event_row,parent,false);
        return new  EventViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        binding.rowEventName.setText(eventPojoList.get(position).getEventName());
        binding.DateTV.setText("Event date :"+eventPojoList.get(position).getDate());
        binding.locationTV.setText("Location :" + eventPojoList.get(position).getLocaton());
        binding.bugetTv.setText("Budget :" + String.valueOf(eventPojoList.get(position).getInitialBudget()) + "  TAKA");
        binding.createEventdate.setText(eventPojoList.get(position).getCreateEventDate());
        String starteventdate = (eventPojoList.get(position).getDate());


        long diffrentDate = EventUtils.getDefferentBetweenTwoDate(EventUtils.getCrrentDate(), starteventdate);
        if (diffrentDate == 0) {
            binding.daysLeft.setText("Coming soon");
        } else if (diffrentDate < 0) {
            binding.daysLeft.setText(" Event Finished");

        } else {
            binding.daysLeft.setText(String.valueOf(EventUtils.getDefferentBetweenTwoDate(EventUtils.getCrrentDate(), starteventdate))+" Days Left");
        }
        binding.menuTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String eventID = eventPojoList.get(position).getEventId();
                final EventPojo eventPojo = eventPojoList.get(position);

                final Bundle bundle = new Bundle();
                bundle.putString("id", eventID);

                PopupMenu popupMenu = new PopupMenu(context, v);
                popupMenu.getMenuInflater().inflate(R.menu.row_menu, popupMenu.getMenu());
                popupMenu.show();

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.detailMenu:
                                Navigation.findNavController(holder.itemView).navigate(R.id.eventDashBoardFragment, bundle);
                                break;
                            case R.id.editMenu:
                                Navigation.findNavController(holder.itemView).navigate(R.id.addEventFragment, bundle);
                                break;
                            case R.id.deleteMenu:
                                eventViewModel.deleteEvent(eventPojo);
                                Navigation.findNavController(holder.itemView).navigate(R.id.event_List_Fragment2);
                                break;

                        }

                        return false;
                    }
                });


            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String eventID=eventPojoList.get(position).getEventId();
                final  EventPojo eventPojo=eventPojoList.get(position);
                final Bundle bundle=new Bundle();
                bundle.putString("id",eventID);
                Navigation.findNavController(holder.itemView).navigate(R.id.eventDashBoardFragment,bundle);


            }
        });

    }

    @Override
    public int getItemCount() {
        return eventPojoList.size();
    }


    public  class EventViewHolder extends RecyclerView.ViewHolder {
        public EventViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

}
