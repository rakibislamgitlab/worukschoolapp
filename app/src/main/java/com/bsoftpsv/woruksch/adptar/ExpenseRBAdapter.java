package com.bsoftpsv.woruksch.adptar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bsoftpsv.woruksch.R;
import com.bsoftpsv.woruksch.databinding.ExpenseRowBinding;
import com.bsoftpsv.woruksch.pojos.Expense;

import java.util.List;

public class ExpenseRBAdapter extends RecyclerView.Adapter<ExpenseRBAdapter.ExpenseViewHolder> {
    private Context context;
    private List<Expense>expenseList;
    private ExpenseRowBinding binding;

    public ExpenseRBAdapter(Context context, List<Expense> expenseList) {
        this.context = context;
        this.expenseList = expenseList;

    }

    @NonNull
    @Override
    public ExpenseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       binding= DataBindingUtil.inflate(LayoutInflater.from(context)
        ,R.layout.expense_row,parent,false);

        return new ExpenseViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull ExpenseViewHolder holder, int position) {
        binding.expenseAmaountRow.setText(""+expenseList.get(position).getExpenseAmount());
        binding.expenseNameRow.setText(expenseList.get(position).getExpenseName());
        binding.expenseDateRow.setText(expenseList.get(position).getExpenseDateTime());


    }

    @Override
    public int getItemCount() {
        return expenseList.size();
    }

    public class ExpenseViewHolder extends RecyclerView.ViewHolder {
        public ExpenseViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
