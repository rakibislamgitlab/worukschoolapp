package com.bsoftpsv.woruksch;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.bsoftpsv.woruksch.adptar.ContractRVA;
import com.bsoftpsv.woruksch.databinding.FragmentAddContractBinding;
import com.bsoftpsv.woruksch.pojos.ContracPojo;
import com.bsoftpsv.woruksch.viewModel.ContractViewModel;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Add_Contract_Fragment extends Fragment {
private ContractViewModel contractViewModel;
private FragmentAddContractBinding binding;
   private String bloodsss = "";
   private ContractRVA contractRVA;
    public Add_Contract_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("           ADD CONTRACT ");
        setHasOptionsMenu(true);
contractViewModel= ViewModelProviders.of(this).get(ContractViewModel.class);
      binding=FragmentAddContractBinding.inflate(LayoutInflater.from(getActivity()));
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(getContext(),R.array.blood
                ,android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
      binding.sppiner.setAdapter(spinnerAdapter);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.sppiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                bloodsss= adapterView.getItemAtPosition(i).toString();
               Toast.makeText(getActivity(), bloodsss, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        binding.studentsaveBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String studentname=binding.stNameEt.getText().toString();
            //   String blood=binding.booltet.getText().toString();
                String batch=binding.batchET.getText().toString();
                String occupation=binding.occapationEt.getText().toString();
                String number=binding.numberET.getText().toString();
                String adress=binding.adressET.getText().toString();
                if (studentname.isEmpty()){
                    binding.stNameEt.setError("Enter  Name");
                    return;
                }
//                if (bloodsss.length()!=0){
//
//                    return;
//                }

                if (occupation.isEmpty()){
                    binding.occapationEt.setError("Enter Occupation");
                    return;
                }
                if (number.isEmpty()){
                    binding.numberET.setError("Enter Number");
                    return;
                }
          //   List<ContracPojo>contracPojoList=new ArrayList<>();
                ContracPojo contracPojo=new ContracPojo(null,studentname,
                        bloodsss,batch,occupation,number,adress);
                contractViewModel.savecontract(contracPojo);
             //   contractRVA.updateList(contracPojoList);

                Navigation.findNavController(view).navigate(R.id.batchContractList);
            }
        });
    }
}
