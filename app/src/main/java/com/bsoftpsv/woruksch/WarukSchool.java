package com.bsoftpsv.woruksch;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;

public class WarukSchool extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
