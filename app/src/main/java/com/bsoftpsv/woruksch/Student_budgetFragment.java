package com.bsoftpsv.woruksch;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.bsoftpsv.woruksch.adptar.BudgetRVA;
import com.bsoftpsv.woruksch.databinding.FragmentStudentBudgetBinding;
import com.bsoftpsv.woruksch.pojos.BudgetPojo;
import com.bsoftpsv.woruksch.viewModel.BudgetViewModel;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Student_budgetFragment extends Fragment {
    private FragmentStudentBudgetBinding binding;
    private BudgetRVA budgetRVA;
    private BudgetViewModel budgetViewModel;

    public Student_budgetFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("        STUDENT TOTAL BUDGET");
        budgetViewModel= ViewModelProviders.of(this).get(BudgetViewModel.class);
        binding = FragmentStudentBudgetBinding.inflate(LayoutInflater.from(getActivity()));
        // Inflate the layout for this fragment
        return binding.getRoot();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.addBudgetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Navigation.findNavController(view).navigate(R.id.
                       action_student_budgetFragment2_to_add_Student_Budget_Fragment);
            }
        });

        budgetViewModel.budgetlist.observe(this, new Observer<List<BudgetPojo>>() {
            @Override
            public void onChanged(List<BudgetPojo> budgetPojos) {
                budgetRVA=new BudgetRVA(getActivity(),budgetPojos);
                LinearLayoutManager llm=new LinearLayoutManager(getActivity());
                binding.budgetRV.setLayoutManager(llm);
                binding.budgetRV.setAdapter(budgetRVA);
                binding.eventProgress.setVisibility(View.GONE);
            }
        });
        binding.budgetRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {

                switch (newState){
                    case RecyclerView.  SCROLL_STATE_IDLE:
                        binding.addBudgetBtn.show();
                        break;
                    default:
                        binding.addBudgetBtn.hide();
                        break;
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
        budgetViewModel.budgetlist.observe(this, new Observer<List<BudgetPojo>>() {
            @Override
            public void onChanged(List<BudgetPojo> budgetPojos) {
                int total=0;
                for (BudgetPojo b :budgetPojos){
                    total +=b.getBudget();
                }
                binding.budgetdtId.setText("Totala Budget :" +total+"/TK");
            }
        });

    }
}
